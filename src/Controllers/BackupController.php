<?php


namespace Invento\Backup\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Backup\Tasks\Backup\BackupJobFactory;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Spatie\Valuestore\Valuestore;

class BackupController extends Controller
{

    public function config(){
        $this->store = Valuestore::make(resource_path('settings/settings.json'));
        $data['status'] = $this->store->has('backup') ? $this->store->get('backup')['status'] : '';
        return view('backup::config',$data);
    }

    public function store(Request $request){
        $this->store = Valuestore::make(resource_path('settings/settings.json'));

        $data['backup'] = [
            'status' => $request->has('status')
        ];

        $this->store->put($data);

        Toastr::success('Successfully Updated',"Backup");

        return back();
    }

    public function backupFiles()
    {

        // Create a backup job configuration from the existing config
        $backupJob = BackupJobFactory::createFromArray(config('backup'));

        // Exclude database backups
     //   $backupJob->dontBackupDatabases();

        // Run the backup job
        $backupJob->run();

        return back();
    }

    public function backupDatabase()
    {

        // Create a backup job configuration from the existing config
        $backupJob = BackupJobFactory::createFromArray(config('backup'));

        // Exclude filesystem backups
       // $backupJob->dontBackupFilesystem();

        // Run the backup job
        $backupJob->run();

        return back();
    }
}