<?php


namespace Invento\Backup;


use Illuminate\Support\ServiceProvider;
use Spatie\Backup\BackupServiceProvider;

class InventoBackupServiceProvider extends ServiceProvider
{
    public function register(){
        $this->app->register(BackupServiceProvider::class);
    }


    public function boot(){
        $this->mergeConfigFrom(__DIR__.'/../config/backup.php', 'backup');


        $this->publishes([
            __DIR__.'/../config/backup.php' => config_path('backup.php')
        ],'backup-config');



        $this->loadViewsFrom(__DIR__.'/../resources/views', 'backup');

        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/backup')
        ],'-backup-views');



        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'backup');

        $this->publishes([
            __DIR__.'/../resources/lang' => $this->app->langPath('vendor/backup')
        ],'backup-lang');



        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

      //  $this->loadRoutesFrom(__DIR__.'/../routes/breadcrumbs.php');
    }
}