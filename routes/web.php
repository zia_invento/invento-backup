<?php

use Illuminate\Support\Facades\Route;
use Invento\Backup\Controllers\BackupController;


Route::middleware(['web', 'auth'])->group(function () {


    Route::get('admin/packages/config-backup', [BackupController::class, 'config'])->name('admin.packages.config-backup');
    Route::post('admin/packages/backup-store', [BackupController::class, 'store'])->name('admin.packages.backup.store');

    Route::get('admin/packages/backup-files', [BackupController::class, 'backupFiles']);
    Route::get('admin/packages/backup-database', [BackupController::class, 'backupDatabase']);


});